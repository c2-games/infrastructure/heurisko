Name:           heurisko
Version:        1.0
Release:        1%{?dist}
Summary:        heurisko

License:        MIT License - Copyright (c) 2024 C2 Games
Source0:        heurisko.tar.gz

BuildRequires:  golang,golang-src,golang-bin,gcc,cpp,gcc-c++,make
#Requires:       

%description
heurisko

%global debug_package %{nil}
%prep
%autosetup


%build
make V=1
sync
#%make_build


%install
%make_install
#rm -rf $RPM_BUILD_ROOT

%post
systemctl daemon-reload

%files
/usr/*
/etc/*



%changelog
* Sat Jan 06 2024 Steve Naimoli <steve@naimolinet.com>
-
