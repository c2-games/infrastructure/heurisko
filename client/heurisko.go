package main

//TODO VERBOSE FLAG CODE

import (
	"bufio"
	"crypto/tls"
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/spf13/pflag"

	"gopkg.in/ini.v1"

	//_ "github.com/mutecomm/go-sqlcipher/v4"
	_ "github.com/mattn/go-sqlite3"
)

// Define your struct types as you did in your initial code

var dbPath string
var serverURL string = ""
var userAgent string = "heurisko/1.0"
var insecureCheck bool = false
var syncRate int = 60 * 5
var configFile string = "-1"
var sshKeyUser string = "-1"
var helpFlag bool
var verboseFlag bool

type UserInfo struct {
	Username  string `json:"username,omitempty"`
	Uid       int    `json:"uid,omitempty"`
	FirstName string `json:"firstName,omitempty"`
	LastName  string `json:"lastName,omitempty"`
	Homedir   string `json:"homedir,omitempty"`
	Gid       int    `json:"gid,omitempty"`
	Shell     string `json:"shell,omitempty"`
}

type GroupInfo struct {
	Name    string `json:"name,omitempty"`
	Gid     int    `json:"gid,omitempty"`
	Members string `json:"members,omitempty"`
}

type UserInfoKey struct {
	Username string `json:"username,omitempty"`
	Uid      int    `json:"uid,omitempty"`
	Sshkey   string `json:"ssh-key,omitempty"`
}

func readConfig() {
	cfg, err := ini.Load(configFile)
	if err != nil {
		log.Fatalf("Failed to load configuration file: %v", err)
	}

	// Access configuration values

	if cfg.Section("server").HasKey("url") {
		serverURL = cfg.Section("server").Key("url").String()
	} else {
		fmt.Println("url is not configured in [server] section of the configuration file.")
		os.Exit(-1)
	}

	if cfg.Section("server").HasKey("insecure") {
		insecureCheck, _ = cfg.Section("server").Key("insecure").Bool()
	} else {
		// Handle the case when "insecure" key is not present
		insecureCheck = false
	}

	if cfg.Section("server").HasKey("syncrate") {
		syncRate, _ = cfg.Section("server").Key("syncrate").Int()
	} else {
		syncRate = 60 * 5
	}

	if cfg.Section("server").HasKey("dbpath") {
		dbPath = cfg.Section("server").Key("dbpath").String()
	} else {
		dbPath = "/var/cache/heurisko/nss.db"
	}

}

func getCreds() string {
	// Read the contents of the /etc/machine-id file into a string.
	machineID, err := os.ReadFile("/etc/machine-id")
	if err != nil {
		log.Fatalf("Failed to read machine ID: %v", err)
	}

	// Convert the byte slice to a string.
	//machineIDStr := string(machineID)
	// Encode as base64 string
	machineIDStr := b64enc(string(machineID))
	now := time.Now().Unix()
	currentTimeString := strconv.FormatInt(now, 10)

	cred := string(machineIDStr + "." + currentTimeString)
	str1 := b64enc(cred)

	return str1
}

func b64enc(value string) string {
	val1 := base64.StdEncoding.EncodeToString([]byte(value))
	return val1
}

func b64dec(value string) string {
	val1, _ := base64.StdEncoding.DecodeString(value)

	return string(val1)
}

func openDB() (*sql.DB, error) {
	//key := url.QueryEscape("secret")
	//dbname := fmt.Sprintf("%s?_pragma_key=%s&_pragma_cipher_page_size=1024", "/tmp/nss.db", key)
	//db, err := sql.Open("sqlite3", dbname)
	db, err := sql.Open("sqlite3", dbPath)

	if err != nil {
		return nil, err
	}
	_, err = db.Exec(`CREATE TABLE users ( Username string PRIMARY KEY, UID INT NOT NULL, GID INT NOT NULL,Dir string NOT NULL,Shell string NOT NULL, firstName string NOT NULL, lastName string NOT NULL, indexUpdate int NOT NULL)`)
	_, err = db.Exec(`CREATE TABLE groups ( Groupname string PRIMARY KEY,GID INT NOT NULL,Members string NOT NULL, indexUpdate int NOT NULL)`)
	//_, err = db.Exec(`CREATE TABLE IF NOT EXISTS groups (gid INTEGER, name TEXT, members TEXT)`)
	//_, err = db.Exec(`CREATE TABLE IF NOT EXISTS users (uid INTEGER, username TEXT, firstName TEXT, lastName TEXT)`)

	return db, nil
}

func getAllUsers(db *sql.DB, indexUpdate int) {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: insecureCheck},
	}

	user_client := &http.Client{Transport: tr}

	req, err := http.NewRequest("GET", serverURL+"/api/allusers", nil)
	if err != nil {
		fmt.Println("Error creating HTTP request:", err)
		return
	}

	req.Header.Set("User-Agent", userAgent)
	req.Header.Set("creds", getCreds())

	resp, err := user_client.Do(req)
	if err != nil {
		fmt.Println("Error making HTTP request:", err)
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		fmt.Println("HTTP request failed with status code:", resp.StatusCode)
		return
	}

	var uInfo []UserInfo
	err = json.NewDecoder(resp.Body).Decode(&uInfo)
	//fmt.Println((err))
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return
	}

	// Use the uInfo data as needed
	for _, user := range uInfo {
		fmt.Printf("%s:%d:%d:%s:%s\n", user.Username, user.Uid, user.Gid, user.Homedir, user.Shell)
		// Add more processing as required
	}

	// Insert data into the "users" table
	for _, user := range uInfo {
		_, err = db.Exec("INSERT or REPLACE INTO users (uid, username, firstName, lastName, gid, dir, shell, indexUpdate) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", user.Uid, user.Username, user.FirstName, user.LastName, user.Gid, user.Homedir, user.Shell, indexUpdate)
		if err != nil {
			fmt.Println("Error inserting data into users table:", err)
			return
		}
	}

	if indexUpdate != 0 {
		_, err = db.Exec("DELETE FROM users WHERE indexUpdate != ?", indexUpdate)
	}
}

func getUser(username string) {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: insecureCheck},
	}

	user_client := &http.Client{Transport: tr}

	req, err := http.NewRequest("GET", serverURL+"/api/getuser/"+username, nil)
	if err != nil {
		fmt.Println("Error creating HTTP request:", err)
		return
	}

	req.Header.Set("User-Agent", userAgent)
	req.Header.Set("creds", getCreds())

	resp, err := user_client.Do(req)
	if err != nil {
		fmt.Println("Error making HTTP request:", err)
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		fmt.Println("HTTP request failed with status code:", resp.StatusCode)
		return
	}

	var uInfo []UserInfo
	err = json.NewDecoder(resp.Body).Decode(&uInfo)
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return
	}

	// Use the uInfo data as needed
	for _, user := range uInfo {
		fmt.Printf("User ID: %d, Username: %s\n", user.Uid, user.Username)
		// Add more processing as required
	}
}

func getUserSSHKey(username string) {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: insecureCheck},
	}

	user_client := &http.Client{Transport: tr}

	req, err := http.NewRequest("GET", serverURL+"/api/getsshkey/"+username, nil)
	if err != nil {
		fmt.Println("Error creating HTTP request:", err)
		return
	}

	req.Header.Set("User-Agent", userAgent)
	req.Header.Set("creds", getCreds())

	resp, err := user_client.Do(req)
	if err != nil {
		fmt.Println("Error making HTTP request:", err)
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		fmt.Println("HTTP request failed with status code:", resp.StatusCode)
		return
	}

	var uInfoKey []UserInfoKey
	err = json.NewDecoder(resp.Body).Decode(&uInfoKey)
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return
	}

	// Use the uInfo data as needed
	for _, user := range uInfoKey {
		//delimiter for multiple ssh-keys
		delimiter := ","
		if strings.Contains(user.Sshkey, delimiter) {
			sshkeys := strings.Split(user.Sshkey, delimiter)
			keycount := len(sshkeys)
			count := 0
			for _, key := range sshkeys {
				fmt.Printf("%s", string(key))
				count = count + 1
				if count < keycount {
					fmt.Printf("\n")
				}
			}
		} else {
			//only one ssh-key attribute
			fmt.Printf("%s", user.Sshkey)
		}
	}
}

func getAllGroups(db *sql.DB, indexUpdate int) {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: insecureCheck},
	}

	user_client := &http.Client{Transport: tr}

	req, err := http.NewRequest("GET", serverURL+"/api/allgroups", nil)
	if err != nil {
		fmt.Println("Error creating HTTP request:", err)
		return
	}

	req.Header.Set("User-Agent", userAgent)
	req.Header.Set("creds", getCreds())

	resp, err := user_client.Do(req)
	if err != nil {
		fmt.Println("Error making HTTP request:", err)
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		fmt.Println("HTTP request failed with status code:", resp.StatusCode)
		return
	}

	var gInfo []GroupInfo
	err = json.NewDecoder(resp.Body).Decode(&gInfo)
	if err != nil {
		fmt.Println("Error decoding JSON:", err)
		return
	}

	// Use the uInfo data as needed
	for _, group := range gInfo {
		fmt.Printf("%d:%s:%s\n", group.Gid, group.Name, group.Members)
		// Add more processing as required
	}

	// Insert data into the "groups" table
	for _, group := range gInfo {
		_, err = db.Exec("INSERT or REPLACE INTO groups (gid, groupname, members, indexUpdate) VALUES (?, ?, ?, ?)", group.Gid, group.Name, group.Members, indexUpdate)
		if err != nil {
			fmt.Println("Error inserting data into groups table:", err)
			return
		}
	}

	if indexUpdate != 0 {
		_, err = db.Exec("DELETE FROM groups WHERE indexUpdate != ?", indexUpdate)
	}
}

func init() {
	pflag.StringVarP(&configFile, "config", "c", "/etc/heurisko_daemon.conf", "Path to the configuration file")
	pflag.StringVarP(&sshKeyUser, "sshkey", "s", "", "Grab SSH Key for Specified User")
	pflag.BoolVarP(&verboseFlag, "verbose", "v", false, "Verbose")
	pflag.BoolVarP(&helpFlag, "help", "h", false, "Display help message")

	pflag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
		pflag.PrintDefaults()
	}
}

func writePIDFile(pid string) {
	filename := "/var/run/heurisko_daemon.pid"

	file, err := os.Create(filename)
	if err != nil {
		fmt.Println("Error creating file:", err)
		return
	}
	defer file.Close()

	writer := bufio.NewWriter(file)

	_, err = writer.WriteString(pid)
	if err != nil {
		fmt.Println("Error writing to file: ", err)
		return
	}

	err = writer.Flush()
	if err != nil {
		fmt.Println("Error flushing file: ", err)
		return
	}
}

func main() {

	pflag.Parse()

	// Check for the help flag
	if helpFlag {
		pflag.Usage()
		return
	}

	readConfig()

	//if len(os.Args) > 1 && os.Args[1] == "--sshkey" {
	if sshKeyUser != "" {
		// This is the daemon process.
		getUserSSHKey(sshKeyUser)
		//os.Exit(0)
		return
	}

	if os.Getenv("DAEMON") == "1" || verboseFlag == true {

		directory := filepath.Dir(dbPath)
		err := os.MkdirAll(directory, os.ModePerm) // You can use a different file mode if needed.

		if err != nil {
			fmt.Printf("Error creating directory: %v\n", err)
			return
		}

		db, err := openDB()
		if err != nil {
			fmt.Println("Error opening database:", err)
			return
		}
		defer db.Close()

		permissionMode := os.FileMode(0755)

		err = os.Chmod(dbPath, permissionMode)
		if err != nil {
			fmt.Printf("Error changing permissions: %v\n", err)
			return
		}

		ticker := time.NewTicker(time.Second * time.Duration(syncRate))
		defer ticker.Stop()

		indexUpdate := 0

		getAllUsers(db, indexUpdate)
		//getUser("user1")
		//getUserSSHKey("user1")
		getAllGroups(db, indexUpdate)

		for {
			select {
			case <-ticker.C:
				getAllUsers(db, indexUpdate)
				getAllGroups(db, indexUpdate)
				indexUpdate = indexUpdate + 1
			}
		}
	} else {
		cmd := exec.Command(os.Args[0], os.Args[1:]...)
		cmd.Env = append(os.Environ(), "DAEMON=1")
		cmd.SysProcAttr = &syscall.SysProcAttr{
			Setsid: true,
		}
		if err := cmd.Start(); err != nil {
			fmt.Println("Error starting process:", err)
			os.Exit(1)
		}
		fmt.Println(cmd.Process.Pid)
		writePIDFile(strconv.Itoa(cmd.Process.Pid))
		os.Exit(0)
	}
}
