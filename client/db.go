package main

import (
	"database/sql"
	"fmt"
	"log"
	"strings"

	"os"
	"os/user"

	. "gitlab.com/c2-games/infrastructure/auth/go-libnss/structs"
	"gopkg.in/ini.v1"

	_ "github.com/mattn/go-sqlite3" // Import go-sqlite3 library
	//_ "github.com/mutecomm/go-sqlcipher/v4"
)

const (
	TOKEN_NOT_FOUND int = iota
)

type GroupMemberinfo struct {
	Username string `usernam:"type,omitempty"`
	// Attributes Attribute `attributes:"type,omitempty"`
}
type UserGroupInfo struct {
	Id   string `id:"type,omitempty"`
	Name string `name:"type,omitempty"`
	Path string `path:"type,omitempty"`
	// Attributes Attribute `attributes:"type,omitempty"`
}
type GroupIdInfo struct {
	// Id         string         `id:"type,omitempty"`
	// Name       string         `name:"type,omitempty"`
	// Path       string         `path:"type,omitempty"`
	Attributes GroupAttribute `attributes:"type,omitempty"`
}

type GroupAttribute struct {
	Gid []string `gid:"type,omitempty"`
	// Gid []string `json:"gid,omitempty"`
}

type UserInfo struct {
	Id         string `id:"type,omitempty"`
	Username   string `username:"type,omitempty"`
	FirstName  string
	LastName   string
	Attributes Attribute `attributes:"type,omitempty"`
}
type Attribute struct {
	Uids     []string `json:"uid,omitempty"`
	Shells   []string `json:"shell,omitempty"`
	Homedirs []string `json:"homedir,omitempty"`
}

var Gid string
var Gid_uint uint
var Uid_uint uint
var Groupmember string

var dbtest_passwd []Passwd
var dbtest_group []Group
var dbtest_shadow []Shadow

var dbPath string = "/var/cache/heurisko/nss.db"
var fullDump bool = false

func readConfig() {

	filename := "heurisko_nss.conf"
	configPath := "/etc/" + filename

	currentUser, err := user.Current()
	if err != nil {
		fmt.Printf("Error getting current user: %v\n", err)
	} else {
		userPath := currentUser.HomeDir + "/." + filename
		// Attempt to get file information
		_, err := os.Stat(userPath)

		// Check if the file exists
		if err == nil {
			configPath = userPath
		}
	}

	cfg, err := ini.Load(configPath)
	if err != nil {
		log.Fatalf("Failed to load configuration file: %v", err)
	}

	if cfg.Section("server").HasKey("dbpath") {
		dbPath = cfg.Section("server").Key("dbpath").String()
	} else {
		dbPath = "/var/cache/heurisko/nss.db"
	}

	if cfg.Section("server").HasKey("fulldump") {
		fullDump, _ = cfg.Section("server").Key("fulldump").Bool()
	} else {
		fullDump = false
	}

}

func init() {
	// file, err := os.OpenFile("/tmp/notes.txt", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)
	// if err != nil {
	// 	panic(err)
	// }
	// defer file.Close()
	//key := url.QueryEscape("secret")
	//dbname := fmt.Sprintf("%s?_pragma_key=%s&_pragma_cipher_page_size=1024", "/tmp/nss.db", key)
	//db, err := sql.Open("sqlite3", dbname)
	readConfig()
	db, err := sql.Open("sqlite3", dbPath)
	if err != nil {
		//log.Fatal(err.Error())   //remove this or ssh will blow up on error
		return
	}
	defer db.Close()

	row, err := db.Query("SELECT username, uid, gid, dir, shell, firstName, lastName FROM users ")
	if err != nil {
		//log.Fatal(err) //remove this or ssh will blow up on error
		return
	}
	defer row.Close()

	for row.Next() { // Iterate and fetch the records from result cursor
		var Username string
		var UID uint
		var GID uint
		var Dir string
		var Shell string
		var first string
		var last string
		row.Scan(&Username, &UID, &GID, &Dir, &Shell, &first, &last)

		// log.Println("Student: ", Username, " ", UID, " ", GID, " ", Dir, " ", Shell)
		dbtest_passwd = append(dbtest_passwd,
			Passwd{
				Username: Username,
				Password: "x",
				UID:      UID,
				GID:      GID,
				Gecos:    first + " " + last,
				//Gecos:    "Test user 1",
				Dir:   Dir,
				Shell: Shell,
			},
		)

		dbtest_shadow = append(dbtest_shadow,
			Shadow{
				Username: Username,
				Password: "!!",
				//Password:        "$6$yZcX.DOY$7bgsJhILMYl3DfMZsYUwoObbVt5Sj9FuujuhVn05Vg9hk.2AXLNy6o1DcPNq0SIyaRZ5YBZer2rYaycuh3qtg1", // Password is "password"
				LastChange:      99999,
				MinChange:       0,
				MaxChange:       99999,
				PasswordWarn:    7,
				InactiveLockout: -1,
				ExpirationDate:  -1,
				Reserved:        -1,
			},
		)

		// fmt.Printf("%+v \n", dbtest_passwd)
	}

	row, err = db.Query("SELECT groupname, gid, members FROM groups ")
	if err != nil {
		log.Fatal(err)
	}
	defer row.Close()
	for row.Next() { // Iterate and fetch the records from result cursor
		var Groupname string
		var GID uint
		var Members string
		row.Scan(&Groupname, &GID, &Members)
		// fmt.Println(Members)
		split := strings.Split(Members, ",")
		split = delete_empty(split)
		//fmt.Println(split)
		Slack := []string{}
		//Slack = append(Slack, "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz")

		for i := 0; i < len(split); i++ {
			// ustString := strings.Join(split[i], " ")
			// fmt.Println(len(split))
			// fmt.Println(split[i])

			Slack = append(Slack, split[i])

		}

		//Slack = append(Slack, "x\000")

		dbtest_group = append(dbtest_group,
			Group{
				Groupname: Groupname,
				Password:  "x",
				GID:       GID,
				Members:   Slack,
			},
		)
	}

	// originalStdout := os.Stdout
	// os.Stdout = file

	// for _, group := range dbtest_group {
	// 	fmt.Fprintf(file, "Group Name: %s\n", group.Groupname)
	// 	fmt.Fprintf(file, "Group Password: %s\n", group.Password)
	// 	fmt.Fprintf(file, "Group ID: %d\n", group.GID)
	// 	fmt.Fprintf(file, "Members:")
	// 	for _, member := range group.Members {
	// 		fmt.Fprintf(file, "  - %s\n", member)
	// 	}
	// 	fmt.Fprintf(file, "\n") // Add a newline to separate groups
	// }

	// // Restore the original standard output
	// os.Stdout = originalStdout
	// fmt.Printf("%+v \n", dbtest_group)

	/*
		dbtest_shadow = append(dbtest_shadow,
			Shadow{
				Username:        "naimols",
				Password:        "$6$yZcX.DOY$7bgsJhILMYl3DfMZsYUwoObbVt5Sj9FuujuhVn05Vg9hk.2AXLNy6o1DcPNq0SIyaRZ5YBZer2rYaycuh3qtg1", // Password is "password"
				LastChange:      99999,
				MinChange:       0,
				MaxChange:       99999,
				PasswordWarn:    7,
				InactiveLockout: -1,
				ExpirationDate:  -1,
				Reserved:        -1,
			},
			Shadow{
				Username:        "web",
				Password:        "$6$yZcX.DOY$7bgsJhILMYl3DfMZsYUwoObbVt5Sj9FuujuhVn05Vg9hk.2AXLNy6o1DcPNq0SIyaRZ5YBZer2rYaycuh3qtg1", // Password is "password"
				LastChange:      99999,
				MinChange:       0,
				MaxChange:       99999,
				PasswordWarn:    7,
				InactiveLockout: 0,
				ExpirationDate:  0,
				Reserved:        -1,
			},
		)

	*/
	// fmt.Printf("%+v \n", dbtest_passwd)
	// fmt.Printf("%+v \n", dbtest_group)
	// fmt.Printf("%+v \n", dbtest_shadow)
}

func delete_empty(s []string) []string {
	var r []string
	for _, str := range s {
		if str != "" {
			r = append(r, str)
		}
	}
	return r
}
