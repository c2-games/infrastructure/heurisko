#!/bin/bash

./clean.sh
go mod init heurisko
go mod tidy
CGO_CFLAGS="-g -O2 -D __LIB_NSS_NAME=heurisko" go build --buildmode=c-shared -o libnss_heurisko.so nss.go db.go
go build heurisko.go
chmod 755 libnss_heurisko.so
chmod 755 heurisko
