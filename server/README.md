```
/etc/heurisko_api.conf
[api]
dbpath = /tmp/heurisko_api_data.db
port = 8888
clockskew = 2
#cacertfile = filepath #not required for ssl
#certfile = filepath #required for ssl
#certkeyfile = filepath #required for ssl

[keycloak]
url = http://heurisko.local:8888
clientid = sampleclientid
clientsecret = SUPERSECRETHASH
realm = samplerealm
sshpermission = sshaccess:yes
syncrate = 60

[defaults]
defaultusergid = 1000
defaultusershell = /bin/bash
defaultuserhomedirpath = /home/{{username}}

[overrides]
uidoverride = uid-override
gidoverride = gid-override
groupgidoverride = gid-override
homediroverride = homedir-override
shelloverride = shell-override


--

/etc/heurisko_api_clients.conf
[client1]
machineid=<<contents of /etc/machine-id of client>>
ipaddress=CLIENTIP
```

