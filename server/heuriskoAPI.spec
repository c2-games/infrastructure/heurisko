Name:           heurisko-api
Version:        1.0
Release:        1%{?dist}
Summary:        heurisko-api

License:        MIT License - Copyright (c) 2024 C2 Games
Source0:        heurisko-api.tar.gz

Requires:       python3

%description
heurisko-api

%global debug_package %{nil}
%prep
%autosetup

%install
%make_install
#rm -rf $RPM_BUILD_ROOT

%post
pip3 install python-keycloak
systemctl daemon-reload

%files
/opt/*
/etc/*



%changelog
* Sat Jan 06 2024 Steve Naimoli <steve@naimolinet.com>
-
