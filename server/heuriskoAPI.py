#!/usr/bin/python3

#threading - https://www.geeksforgeeks.org/multithreading-python-set-1/
#daemons - http://web.archive.org/web/20131017130434/http://www.jejik.com/articles/2007/02/a_simple_unix_linux_daemon_in_python/

#requires pip3 install python-keycloak


import base64
import configparser
import json
import os
import socket
import sqlite3
import subprocess
import sys
import threading
import time
import datetime
import traceback
import ssl
import argparse
from http.server import BaseHTTPRequestHandler, HTTPServer

basepath = os.path.dirname(__file__)
sys.path.insert(0, basepath + '/lib')

from _daemon import daemon
from _kcmgr import KeycloakManager

current_user = int(os.geteuid())
runningThread = True
started = False
pidfile = '/var/run/heurisko_api.pid'
HEURISKO_DATA_DIR = "/etc/"
server_name = "Heurisko API Daemon"
server_version = "1.0.0"
www_port = 8888
kc = 0
cfg = 0
syncrate = 60
indexUpdate = 0
db_path = '/tmp/heurisko_api_data.db'
clockskew = 2
sslenable = False
certfile = ""
certkeyfile = ""
cacertfile = ""

def readConfig():
    global syncrate
    global db_path
    global www_port
    global clockskew
    global sslenable
    global certfile
    global certkeyfile
    global cacertfile

    config = configparser.ConfigParser(default_section=None)
    config.read(HEURISKO_DATA_DIR + "/heurisko_api.conf")
    if not (('url' in config['keycloak']) and ('clientid' in config['keycloak']) and ('clientsecret' in config['keycloak']) and ('realm' in config['keycloak']) and ('sshpermission' in config['keycloak'])):
        print("Missing required configuration in [keycloak] section...")
        sys.exit()
    elif not (('defaultusergid' in config['defaults']) and ('defaultusershell' in config['defaults']) and ('defaultuserhomedirpath' in config['defaults'])):
        print("Missing required configuration in [defaults] section...")
        sys.exit()

    if not 'overrides' in config:
        config['overrides']['uidoverride'] = ""
        config['overrides']['groupgidoverride'] = ""
        config['overrides']['gidoverride'] = ""
        config['overrides']['homediroverride'] = ""
    else:
        if not 'uidoverride' in config['overrides']:
            config['overrides']['uidoverride'] = ""

        if not 'groupgidoverride' in config['overrides']:
            config['overrides']['groupgidoverride'] = ""

        if not 'gidoverride' in config['overrides']:
            config['overrides']['gidoverride'] = ""
        
        if not 'homediroverride' in config['overrides']:
            config['overrides']['homediroverride'] = ""

    if not 'syncrate' in config['keycloak']:
        config['keycloak']['syncrate'] = syncrate
    else:
        syncrate = int(config['keycloak']['syncrate'])

    if 'api' in config:
        if not 'dbpath' in config['api']:
            config['api']['dbpath'] = db_path
        else:
            db_path = config['api']['dbpath']
        
        if not 'port' in config['api']:
            config['api']['port'] = int(www_port)
        else:
            www_port = int(config['api']['port'])

        if not 'clockskew' in config['api']:
            config['api']['clockskew'] = int(clockskew)
        else:
            clockskew = int(config['api']['clockskew'])

        if 'certfile' and 'certkeyfile' in config['api']:
            if os.path.exists(config['api']['certfile']) and os.path.exists(config['api']['certkeyfile']):
                sslenable = True
                certfile = config['api']['certfile']
                certkeyfile = config['api']['certkeyfile']
                print("SSL Enabled")
                if 'cacertfile' in config['api']:
                    if os.path.exists(config['api']['cacertfile']):
                        cacertfile = config['api']['cacertfile']
                        print("SSL Enabled with CA Cert")
            else:
                print("Not enabling SSL, missing one of the required files.")
    else:
        config['api']['dbpath'] = db_path
        config['api']['port'] = int(www_port)
        config['api']['clockskew'] = int(clockskew)
    return config

class MyDaemon(daemon):
    def run(self):
        global cfg
        while True:
            global started
            if started == False:
                started = True
                cfg = readConfig() #also re-read on every iteration of KC Daemon
                if os.path.exists(db_path):
                    os.remove(db_path)

                t1 = WebServerThread("www")
                t2 = KCSync()

                t1.start()
                t2.start()

                t1.join()
                t2.join()

            time.sleep(1)

class KCSync(threading.Thread):
    def run(self):
        global cfg
        global kc
        global indexUpdate
        while runningThread:
            cfg = readConfig() #incase someone wants to change without restarting
            kc = KeycloakManager(cfg,db_path,indexUpdate)
            kc.populateStructures()  
            time.sleep(syncrate)
            indexUpdate = indexUpdate + 1

class StaticHTTPServer(BaseHTTPRequestHandler):
    def execute_request(self):
        self.server_version = server_name
        self.sys_version = server_version
        
        (client, cport) = self.client_address
        if self.command == "GET":
            if self.path == "/index.html" or self.path == "/":
                #self.serveCode(200, "text", "Hello World")
                self.serveCode(200,'html', "<center><h1>You should not be here</h1></center>")
            # elif self.path.endswith('.png'):
            #     img = readImage(self.path)
            #     self.serveImage(img)
            elif self.path.startswith('/api/get'):
                args = self.path.split('/')
                if args[2].lower() in ['getuser', 'getgroup', 'getsshkey']:
                    if len(args[3]) > 0:
                        command = args[2].lower()
                        objectName = args[3].lower()

                        if command == "getuser":
                            if self.checkAuth() == True:
                                u = self.dbAction("getuser", objectName)
                                self.serveCode(200, "json", u)
                            else:
                                self.serveCode(401, "text", "unauthorized")

                        elif command == "getgroup":
                            if self.checkAuth() == True:
                                g = self.dbAction("getgroup", objectName)
                                self.serveCode(200, "json", g)
                            else:
                                self.serveCode(401, "text", "unauthorized")

                        elif command == "getsshkey":
                            if self.checkAuth() == True:
                                u = self.dbAction("getusersshkey", objectName)
                                self.serveCode(200, "json", u)
                            else:
                                self.serveCode(401, "text", "unauthorized")

                    else:
                        self.serveCode(404, "text", "need to pass command and object")

            elif self.path.startswith('/api/all'):
                args = self.path.split('/')
                if args[2].lower() in ['allusers', 'allgroups']:
                    command = args[2].lower()
                    if command == "allusers":
                        if self.checkAuth() == True:
                            u = self.dbAction("allusers")
                            self.serveCode(200, "json", u)
                        else:
                            self.serveCode(401, "text", "unauthorized")

                    elif command == "allgroups":
                        if self.checkAuth() == True:
                            g = self.dbAction("allgroups")
                            self.serveCode(200, "json", g)
                        else:
                            self.serveCode(401, "text", "unauthorized")

            else:
                self.serveCode(404, "text", "Not Found")

    def checkAuth(self):
        now = int(unixTime())
        (clientip, cport) = self.client_address
        clients = configparser.ConfigParser(default_section=None)
        clients.read(HEURISKO_DATA_DIR + "/heurisko_api_clients.conf")

        if 'creds' in self.headers and 'user-agent' in self.headers:
            creds = base64decode(self.headers['creds'])
            ua = self.headers['user-agent']

            (machineidb64, cli_time) = creds.split(".")
            machineid = base64decode(machineidb64)
            machineid = machineid.strip()
            machineid = machineid.replace("\n", "")

            for client in clients.sections():
                if clients[client]['ipaddress'] == clientip:
                    if clients[client]['machineid'] == machineid:
                        cli_time = int(cli_time)
                        for i in range(now - clockskew,now + clockskew):
                            if i == cli_time:
                                return True
                    
            return False


    def serveCode(self, code, type, message):
        self.send_response(code)
        
        if type == "text":
            self.send_header('Content-Type', 'text/plain')
        elif type == "html":
            self.send_header('Content-Type', 'text/html')
        elif type == "json":
            self.send_header('Content-Type', 'application/json')
        
        self.send_header('Cache-Control', "no-cache, no-store, must-revalidate")
        self.send_header('Pragma', "no-cache")
        self.send_header('Expires', "0")

        self.end_headers()
        
        self.wfile.write(bytearray(message, 'utf-8'))

    def do_POST(self):
        self.execute_request()
    
    def do_GET(self):
        self.execute_request()

    def dbAction(self, action, param=""):
        db = self.connectToDB(db_path)
        ret = ""
        if action == "allgroups":
            g = self.get_all_groups(db)
            g = json.dumps(g)
            ret = g
        elif action == "allusers":
            u = self.get_all_users(db)
            u = json.dumps(u)
            ret = u
        elif action == "getgroup":
            g = self.get_group(db, param)
            g = json.dumps(g)
            ret = g
        elif action == "getuser":
            u = self.get_user(db, param)
            u = json.dumps(u)
            ret = u    
        elif action == "getusersshkey":
            u = self.get_user_sshkey(db, param)
            u = json.dumps(u)
            ret = u       

        self.closeDB(db)
        return ret

    def connectToDB(self,path):
        conn = sqlite3.connect(path)
        cursor = conn.cursor()
        db = [conn, cursor]

        return db
    
    def closeDB(self,db):
        db[1].close()
        db[0].close()

    def get_all_users(self, db):
        cursor = db[1]
        cursor.execute("SELECT json_group_array(json_object('uid', uid, 'username', username, 'firstName', firstName, 'lastName', lastName, 'homedir', homedir, 'gid', gid, 'shell', shell)) FROM unixpasswd where indexUpdate = ?", (indexUpdate, ))
        users = cursor.fetchall()[0][0]
        users = json.loads(users)
        return users

    def get_user(self, db, user):
        cursor = db[1]
        cursor.execute("SELECT json_group_array(json_object('uid', uid, 'username', username, 'firstName', firstName, 'lastName', lastName, 'homedir', homedir, 'gid', gid, 'shell', shell)) FROM unixpasswd where LOWER(username) = LOWER(?) and indexUpdate = ?", (user.lower(),indexUpdate, ))
        users = cursor.fetchall()[0][0]
        users = json.loads(users)
        return users

    def get_user_sshkey(self, db, user):
        cursor = db[1]
        cursor.execute("SELECT json_group_array(json_object('uid', uid, 'username', username, 'ssh-key', ssh_key)) FROM unixpasswd where LOWER(username) = LOWER(?) and indexUpdate = ?", (user.lower(),indexUpdate,))
        users = cursor.fetchall()[0][0]
        users = json.loads(users)[0]
        users['ssh-key'] = json.loads(users['ssh-key'])
        users['ssh-key'] = ",".join(users['ssh-key'])
        u = []
        u.append(users)
        return u

    def get_all_groups(self,db):
        cursor = db[1]
        cursor.execute("SELECT json_group_array(json_object('name', name, 'gid', gid, 'members', members)) FROM unixgroup where indexUpdate = ?", (indexUpdate,))
        groups = cursor.fetchall()[0][0]
        groups = json.loads(groups)
        for group in groups:
            group['members'] = json.loads(group['members'])
            group['members'] = ",".join(group['members'])
        return groups

    def get_group(self,db, group):
        cursor = db[1]
        cursor.execute("SELECT json_group_array(json_object('name', name, 'gid', gid, 'members', members)) FROM unixgroup where LOWER(name) = LOWER(?) and indexUpdate = ?", (group.lower(), indexUpdate,))

        groups = cursor.fetchall()[0][0]
        groups = json.loads(groups)[0]

        groups['members'] = json.loads(groups['members'])
        groups['members'] = ",".join(groups['members'])
        return groups

class WebServerThread(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name + "-Thread"

    def run(self):
        while runningThread:
            www_server_address = ("0.0.0.0", www_port)
            httpd = HTTPServer(www_server_address, StaticHTTPServer)
            print('Starting Server on port {}'.format(www_port))

            if sslenable == True:
                certreq = ssl.CERT_OPTIONAL
                if len(cacertfile) > 0:
                    httpd.socket = ssl.wrap_socket(httpd.socket, server_side=True, ca_certs=cacertfile, certfile=certfile, keyfile = certkeyfile, ssl_version=ssl.PROTOCOL_TLSv1_2,cert_reqs=certreq)
                else:
                    httpd.socket = ssl.wrap_socket(httpd.socket, server_side=True, certfile=certfile, keyfile = certkeyfile, ssl_version=ssl.PROTOCOL_TLSv1_2,cert_reqs=certreq)

            httpd.serve_forever()

def base64encode(message):
    msgb = message.encode('ascii')
    b64b = base64.b64encode(msgb)
    b64m = b64b.decode('ascii')
    return b64m

def base64decode(hash):
    b64b = hash.encode('ascii')
    msgb = base64.b64decode(b64b)
    msg = msgb.decode('ascii')
    return msg

def unixTime():
    presentDate = datetime.datetime.now()
    unix_timestamp = datetime.datetime.timestamp(presentDate)
    return unix_timestamp

def main():    
    global runningThread
    global kc

    if len(sys.argv) == 1:
        print("Options: stop, [debug]start, test")
        sys.exit()
    arg = sys.argv[1]
    daemon = MyDaemon(pidfile)
    if arg == "start":
        runningThread = True
        daemon.start(debug=False)
    elif arg == "debugstart":
        runningThread = True     
        daemon.start(debug=True)
    elif arg == "stop":
        cfg = readConfig()
        runningThread = False
        daemon.stop()
    elif arg == "test":
        cfg = readConfig() #incase someone wants to change without restarting
        kc = KeycloakManager(cfg,db_path,indexUpdate, test=True)
        kc.populateStructures(test=True)  
if __name__ == "__main__":
    if current_user == 0:
        main()
    else:
        print("Access Denied!")
