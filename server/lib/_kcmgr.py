import sqlite3
from keycloak import KeycloakAdmin
import sys
import os
import requests
import json
import pprint
import hashlib

class KeycloakManager:
    def __init__(self, cfg, db_path, indexUpdate, test=False):
        #[keycloak]
        self.kc_url = cfg['keycloak']['url']
        self.kc_clientid = cfg['keycloak']['clientid']
        self.kc_clientsecret = cfg['keycloak']['clientsecret']
        self.kc_realm = cfg['keycloak']['realm']
        self.kc_group_ssh_attribute = cfg['keycloak']['sshpermission']
        #[overrides]
        self.kc_group_gid_override = cfg['overrides']['groupgidoverride']
        self.kc_user_uid_override = cfg['overrides']['uidoverride']
        self.kc_user_gid_override = cfg['overrides']['gidoverride']
        self.kc_user_homedir_override = cfg['overrides']['homediroverride']
        self.kc_user_shell_override = cfg['overrides']['shelloverride']
        #[defaults]
        self.kc_user_default_gid = cfg['defaults']['defaultusergid']
        self.kc_user_default_shell = cfg['defaults']['defaultusershell']
        self.kc_user_default_homedirpath = cfg['defaults']['defaultuserhomedirpath']
        self.indexUpdate = indexUpdate
        self._showdupes = 0
        self.unixgroups = {}
        self.unixpasswd = {}
        self.conn = 0
        self.test = test

        if test == False:
            self.create_database(db_path)

        self.kca = KeycloakAdmin(server_url=self.kc_url, client_id=self.kc_clientid, client_secret_key=self.kc_clientsecret, realm_name=self.kc_realm, verify=True, auto_refresh_token=["get", "put", "post", "delete"])

    def connect_to_kc(self):
        headers = self.kca.connection.headers
        if self.test == True:
            print("Header Information:")
            print(headers)
        return headers

    def create_database(self, db_path):
        # Create a SQLite database and two tables
        self.conn = sqlite3.connect(db_path)
        self.cursor = self.conn.cursor()
        # self.cursor.execute('''
        #     CREATE TABLE IF NOT EXISTS unixgroup (
        #         id INTEGER PRIMARY KEY AUTOINCREMENT,
        #         name TEXT,
        #         gid INTEGER,
        #         members TEXT,
        #         indexUpdate INTEGER
        #     )
        # ''')
        # self.cursor.execute('''
        #     CREATE TABLE IF NOT EXISTS unixpasswd (
        #         id INTEGER PRIMARY KEY AUTOINCREMENT,
        #         uid INTEGER,
        #         username TEXT,
        #         firstName TEXT,
        #         lastName TEXT,
        #         ssh_key TEXT,
        #         homedir TEXT,
        #         gid INTEGER,
        #         shell TEXT,
        #         indexUpdate INTEGER
        #     )
        # ''')

        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS unixgroup (
                name TEXT,
                gid INTEGER PRIMARY KEY,
                members TEXT,
                indexUpdate INTEGER
            )
        ''')
        self.cursor.execute('''
            CREATE TABLE IF NOT EXISTS unixpasswd (
                uid INTEGER PRIMARY KEY,
                username TEXT,
                firstName TEXT,
                lastName TEXT,
                ssh_key TEXT,
                homedir TEXT,
                gid INTEGER,
                shell TEXT,
                indexUpdate INTEGER
            )
        ''')
        self.conn.commit()
        permission_mode = 0o600
        os.chmod(db_path, permission_mode)

    def save_unixgroups_to_db(self):
        for group_name, group_data in self.unixgroups.items():
            members = json.dumps(group_data['members'])
            self.cursor.execute("INSERT OR REPLACE INTO unixgroup (name, gid, members, indexUpdate) VALUES (?, ?, ?, ?)", (group_name, group_data['gid'], members, self.indexUpdate))
        self.conn.commit()

    def save_unixpasswd_to_db(self):
        for uid, user_data in self.unixpasswd.items():
            ssh_key = json.dumps(user_data['ssh-key'])  # Convert to JSON string
            self.cursor.execute("INSERT OR REPLACE INTO unixpasswd (uid, username, firstName, lastName, ssh_key, homedir, gid, shell, indexUpdate) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                                (uid, user_data['username'], user_data['firstName'], user_data['lastName'], ssh_key, user_data['homedir'], user_data['gid'], user_data['shell'], self.indexUpdate))
        self.conn.commit()

    # def get_all_groups(self):
    #     self.cursor.execute("SELECT * FROM unixgroup")
    #     groups = self.cursor.fetchall()
    #     group_list = []
    #     for group in groups:
    #         group_data = {
    #             'name': group[1],
    #             'gid': group[2]
    #         }
    #         group_list.append(group_data)
    #     return group_list

    # def get_group_by_name(self, group_name):
    #     self.cursor.execute("SELECT * FROM unixgroup WHERE name = ?", (group_name,))
    #     group = self.cursor.fetchone()
    #     if group:
    #         group_data = {
    #             'name': group[1],
    #             'gid': group[2]
    #         }
    #         return group_data
    #     else:
    #         return None

    # def get_all_users(self):
    #     self.cursor.execute("SELECT * FROM unixpasswd")
    #     users = self.cursor.fetchall()
    #     user_list = []
    #     for user in users:
    #         user_data = {
    #             'uid': user[1],
    #             'username': user[2],
    #             'firstName': user[3],
    #             'lastName': user[4],
    #             'ssh-key': user[5],
    #             'homedir': user[6]
    #         }
    #         user_list.append(user_data)
    #     return user_list

    # def get_user_by_username(self, username):
    #     self.cursor.execute("SELECT * FROM unixpasswd WHERE username = ?", (username,))
    #     user = self.cursor.fetchone()
    #     if user:
    #         user_data = {
    #             'uid': user[1],
    #             'username': user[2],
    #             'firstName': user[3],
    #             'lastName': user[4],
    #             'ssh-key': user[5],
    #             'homedir': user[6]
    #         }
    #         return user_data
    #     else:
    #         return None

    def get_ssh_groups(self, header):
        url = f"{self.kca.server_url}/admin/realms/{self.kca.realm_name}/groups?q={self.kc_group_ssh_attribute}&exact=true&briefRepresentation=false"
        r = requests.get(url, headers=header)
        if r.status_code == 200:
            groupsraw = r.content.decode('utf-8')
            groups = json.loads(groupsraw)
            return groups
        else:
            print("Error: " + str(r.status_code))
            return -1

    def merge_dict(self, dict1, dict2):
        rest = dict1 | dict2
        return rest

    def uuid_to_unix_uid(self, uuid, starting_uid=1000000):
        uuid_bytes = uuid.encode('utf-8')
        hashed_value = int(hashlib.sha256(uuid_bytes).hexdigest(), 16)
        uid_range = 4294967295 - starting_uid
        unix_uid = starting_uid + (hashed_value % uid_range)
        return unix_uid

    def process_group(self, group, sub=False):
        members = {}
        x = self.kca.get_group_members(group_id=group['id'])
        members[group['name']] = []
        for i in x:
            if i['emailVerified'] == True and i['enabled'] == True:
                user = {}
                user['uid'] = self.uuid_to_unix_uid(i['id'])
                user['username'] = i['username']
                user['firstName'] = i['firstName']
                user['lastName'] = i['lastName']
                user['homedir'] = str(self.kc_user_default_homedirpath).replace("{{username}}", i['username'])
                user['gid'] = self.kc_user_default_gid
                user['shell'] = self.kc_user_default_shell

                if 'attributes' in i:
                    if self.kc_user_uid_override in i['attributes']:
                        user['uid'] = i['attributes'][self.kc_user_uid_override][0]
                    
                    if self.kc_user_homedir_override in i['attributes']:
                        user['homedir'] = i['attributes'][self.kc_user_homedir_override][0]

                    if self.kc_user_gid_override in i['attributes']:
                        user['gid'] = i['attributes'][self.kc_user_gid_override][0]

                    if self.kc_user_shell_override in i['attributes']:
                        user['shell'] = i['attributes'][self.kc_user_shell_override][0]

                    if 'ssh-key' in i['attributes']:
                        user['ssh-key'] = i['attributes']['ssh-key']
                    else:
                        user['ssh-key'] = ""
                else:
                    user['ssh-key'] = ""
                members[group['name']].append(user)

        if len(members[group['name']]) > 0:
            self.unixgroups[group['name']] = {}
            self.unixgroups[group['name']]['gid'] = self.uuid_to_unix_uid(i['id'])
            self.unixgroups[group['name']]['name'] = group['name']
            self.unixgroups[group['name']]['members'] = []

            if 'attributes' in group:
                if self.kc_group_gid_override in group['attributes']:
                    self.unixgroups[group['name']]['gid'] = group['attributes'][self.kc_group_gid_override][0]

        if len(group['subGroups']) > 0:
            subs = group['subGroups']
            for j in subs:
                submembs = self.process_group(j, sub=True)
                members = self.merge_dict(members, submembs)

        return members

    def pp4me(self, str1):
        pp = pprint.PrettyPrinter(depth=4)
        pp.pprint(str1)

    def populateStructures(self, test=False):
        headers = self.connect_to_kc()
        groups = self.get_ssh_groups(headers)
        users = {}
        for i in groups:
            tmpuser = self.process_group(i, sub=False)
            users = self.merge_dict(users, tmpuser)

        for group in users:
            for mem in users[group]:
                self.unixpasswd[mem['uid']]= {}
                self.unixpasswd[mem['uid']]= mem
                self.unixgroups[group]['members'].append(mem['username'])

        if test == False:
            self.save_unixgroups_to_db()
            self.save_unixpasswd_to_db()
        else:
            print("NSS Enabled Groups:")
            for group in self.unixgroups:
                print("\t" + group + "\t" + str(self.unixgroups[group]['gid']))
            print("NSS Enabled Users:")
            for user in self.unixpasswd:
                print("\t" + str(user) + "\t" + self.unixpasswd[user]['username'])