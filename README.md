# heurisko

```
"Heurisko" is a Greek word (εὑρίσκω) that means "I find" or "I discover."

client (heurisko) - heurisko daemon which communicates with the heurisko API server and libnss client
server (heurisko-api) - heurisko API server which communicates to the specified Keycloak Server

In "production use," it should be refrained from running both the client and the API server on the same node.

```
